# Micro service example

Offers a REST API to CRUD a User, on top of a DB table. Runs a job periodically to create a new user.

## How to run

It can run in two ways: as a standalone server exposing an API, or as a standalone app which runs a job every 3 minutes.

Api mode:
go run api/main/main.go -config="path to config file" api

Cronjob mode:
go run api/main/main.go -config="path to config file" job

## Functionality

This µS offers the following functionality:

* in API mode, it exposes a REST API to manage user objects.
* in cronjob mode, it runs every 3 minutes executing a workflow composed of:
  * creates a new user object and writes it to DB
  * sends a MS Teams notification about that newly created user
  * sends a mock email about a mock user which is about to expire in one day
