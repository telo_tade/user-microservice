package rest

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/go-log/log"
	"github.com/gorilla/mux"
	"gitlab.com/telo_tade/golang_micro_service/pkg/models"
	"gitlab.com/telo_tade/golang_micro_service/pkg/service"
)

// Handler stores common objects needed by all handlers.
type Handler struct {
	Logger  log.Logger
	Service service.Service
}

// GetAllUsers returns a user by its ID.
func (h *Handler) GetAllUsers(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	defer h.logDuration(now)
	h.Logger.Logf("GetAllUsers: %v", now)

	user, err := h.Service.GetAllUsers(r.Context())
	if err != nil {
		h.Logger.Logf("Could not retrieve users: %v.", err)
		w.WriteHeader(http.StatusNotFound)

		return
	}

	h.writeJSONResponse(user, http.StatusOK, w)
}

// GetUser returns a user by its ID.
func (h *Handler) GetUser(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	defer h.logDuration(now)
	h.Logger.Logf("GetUser: %v", now)

	params := mux.Vars(r)
	userIDParam, ok := params["ID"]

	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Logf("Request does not contain a valid user ID parameter.")

		return
	}

	userID, err := strconv.Atoi(userIDParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Logf("Request does not contain a valid user ID parameter.")

		return
	}

	user, err := h.Service.GetUser(r.Context(), uint(userID))
	if err != nil {
		h.Logger.Logf("Could not retriver user with ID %v: %v.", userIDParam, err)
		w.WriteHeader(http.StatusNotFound)

		return
	}

	h.writeJSONResponse(user, http.StatusOK, w)
}

// CreateUser creates a new user.
func (h *Handler) CreateUser(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	defer h.logDuration(now)
	h.Logger.Logf("CreateUser: %v", now)

	u := models.User{}

	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		h.Logger.Logf("Could not decode user: %v", err)
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	err = h.Service.CreateUser(r.Context(), u)
	if err != nil {
		h.Logger.Logf("Could not create user: %v.", err)
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	h.writeJSONResponse(u, http.StatusCreated, w)
}

// HealthCheck returns "pong".
func (h *Handler) HealthCheck(w http.ResponseWriter, r *http.Request) {
	if _, err := w.Write([]byte("pong")); err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		h.Logger.Logf("error writing response body, %v", err)
	}
}

func (h *Handler) writeJSONResponse(resp interface{}, code int, w http.ResponseWriter) {
	w.WriteHeader(code)
	w.Header().Set("Content-Type", "application/json")

	body, err := json.Marshal(resp)
	if err != nil {
		h.Logger.Logf("error when marshalling result to json, %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	_, err = w.Write(body)
	if err != nil {
		h.Logger.Logf("error writing response body, %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}
}

func (h *Handler) logDuration(start time.Time) {
	h.Logger.Logf("Started at %v, It took %v", start, time.Since(start))
}
