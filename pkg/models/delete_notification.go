package models

import (
	"fmt"
	"time"
)

// DeleteNotification models the recipient of a delete notification email.
type DeleteNotification struct {
	Email        string
	Name         string
	DeletionDate time.Time
	LastNotified time.Time
}

func (r DeleteNotification) String() string {
	return fmt.Sprintf("DeleteNotification[Email %v, Name %v, DeletionDate %v, LastNotified %v]",
		r.Email, r.Name, r.DeletionDate, r.LastNotified)
}
