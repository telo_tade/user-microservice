/*
Package scheduler will create a CRON job, running at intervals configured by the user.
This job runs and each time executes a function given as a parameter. Its main method is:
	Run(cronExpression string, task func())
Function Run will run forever, every time the cronExpression is valid
*/
package scheduler

import (
	"time"

	"github.com/go-log/log"
	"github.com/robfig/cron"
)

type flow interface {
	ExecuteFlow()
}

// Client allows scheduling repeated runs a function.
type Client struct {
	CronExpression string
	Logger         log.Logger
	Flow           flow
}

// Run will spawn a goroutine running the task function, every frequency seconds.
func (c *Client) Run() {
	cronJob := cron.New()

	err := cronJob.AddFunc(c.CronExpression, func() {
		c.Logger.Logf("Starting a new scheduled run: %v.", time.Now())

		c.Flow.ExecuteFlow()
	})
	if err != nil {
		panic("unable to add task to scheduler")
	}

	cronJob.Start()
}
