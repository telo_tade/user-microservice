// +build !race

package scheduler_test

import (
	"testing"
	"time"

	"github.com/go-log/log"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/golang_micro_service/pkg/scheduler"
)

func TestClient_Run(t *testing.T) {
	type fields struct {
		CronExpression string
		Logger         log.Logger
		Flow           *flowMock
	}

	tests := []struct {
		name          string
		fields        fields
		sleep         time.Duration
		expectedCalls int
	}{
		{
			name: "Runs 1.2 seconds calls ExecuteFlow once",
			fields: fields{
				CronExpression: "*/1 * * ? * * ", Flow: &flowMock{}, Logger: &logMock{},
			},
			sleep:         1200 * time.Millisecond,
			expectedCalls: 1,
		},
		{
			name: "Runs 2.2 seconds calls ExecuteFlow twice",
			fields: fields{
				CronExpression: "*/1 * * ? * * ", Flow: &flowMock{}, Logger: &logMock{},
			},
			sleep:         2200 * time.Millisecond,
			expectedCalls: 2,
		},
		{
			name: "Runs 0.99 seconds does not call ExecuteFlow",
			fields: fields{
				CronExpression: "*/1 * * ? * * ", Flow: &flowMock{}, Logger: &logMock{},
			},
			sleep:         990 * time.Millisecond,
			expectedCalls: 0,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := scheduler.Client{
				CronExpression: tt.fields.CronExpression,
				Logger:         tt.fields.Logger,
				Flow:           tt.fields.Flow,
			}

			now := time.Now()
			start := now.Truncate(time.Second).Add(1 * time.Second)
			time.Sleep(start.Sub(now)) // wake up close after the beginning of a new second

			go c.Run()

			time.Sleep(tt.sleep)

			assert.Equal(t, tt.expectedCalls, tt.fields.Flow.called, "Should be called exactly %d times.", tt.expectedCalls)
		})
	}
}

type flowMock struct {
	called int
}

func (f *flowMock) ExecuteFlow() {
	f.called++
}

type logMock struct{} // normal log would crowd the screen with multiple goroutines calling

func (l *logMock) Log(v ...interface{})                 {}
func (l *logMock) Logf(format string, v ...interface{}) {}
