package commands

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"syscall"

	"github.com/google/subcommands"
)

// JobCmd represents a cmd line command.
type JobCmd struct {
	Scheduler runner
}

type runner interface {
	Run()
}

// Name returns the name of the command.
func (j *JobCmd) Name() string { return "job" }

// Synopsis returns a short string (less than one line) describing the command.
func (j *JobCmd) Synopsis() string { return "Start this app in cronjob mode." }

// Usage returns a long string explaining the command and giving usage information.
func (j *JobCmd) Usage() string {
	return "Start the app in command line mode. This will cause the app to create a new User every 3 minutes.\n" +
		"This user gets saved in a DB, a Teams notification is pushed and a notification email is sent."
}

// SetFlags adds the flags for this command to the specified set.
func (j *JobCmd) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (j *JobCmd) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	j.Scheduler.Run()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	<-sig

	return subcommands.ExitSuccess
}
