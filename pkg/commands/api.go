package commands

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"syscall"

	"github.com/google/subcommands"
)

// APICmd represents a cmd line command.
type APICmd struct {
	Server starter
}

type starter interface {
	Start()
}

// Name returns the name of the command.
func (*APICmd) Name() string { return "api" }

// Synopsis returns a short string (less than one line) describing the command.
func (*APICmd) Synopsis() string { return "Start this app in API mode." }

// Usage returns a long string explaining the command and giving usage information.
func (*APICmd) Usage() string {
	return "Start the app in API mode. This will start a server which provides a rest API.\n" +
		"Its purpose is to manipulate a User object, offering CRUD functionality around a DB table."
}

// SetFlags adds the flags for this command to the specified set.
func (*APICmd) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (s *APICmd) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	s.Server.Start()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	<-sig

	return subcommands.ExitSuccess
}
