package email_test

import (
	"reflect"
	"testing"
	"time"

	"github.com/mailjet/mailjet-apiv3-go"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/golang_micro_service/pkg/email"
	"gitlab.com/telo_tade/golang_micro_service/pkg/models"
)

func TestComputeDeleteMessages(t *testing.T) {
	recipients := []models.DeleteNotification{
		{Email: "john.1@metronom.com", Name: "John Doe 1", DeletionDate: time.Now().Add(240 * time.Hour)},
		{Email: "john.2@metronom.com", Name: "John Doe 2", DeletionDate: time.Now().Add(264 * time.Hour)},
		{Email: "john.3@metronom.com", Name: "John Doe 3", DeletionDate: time.Now().Add(288 * time.Hour)},
	}

	client := email.Client{}
	result := client.ComputeDeleteMessages(recipients)

	expected1 := mailjet.InfoMessagesV31{
		From:                     &mailjet.RecipientV31{Email: "", Name: ""},
		DeduplicateCampaign:      false,
		StatisticsContactsListID: 0,
		Subject:                  "Your account is about to expire",
		TemplateErrorDeliver:     false,
		TemplateID:               0,
		TemplateLanguage:         true,
		To:                       &mailjet.RecipientsV31{{Email: "john.1@metronom.com", Name: "John Doe 1"}},
		Variables: map[string]interface{}{
			"CustomerName": "John Doe 1", "DeletionDate": email.FormatDate(recipients[0].DeletionDate),
			"DeletionInDays": "10", "DeletionPreventionInDays": "9",
		},
	}

	assert.True(t, reflect.DeepEqual(expected1, result[0]), "Wrong result, expected1 != result[0]")

	expected2 := mailjet.InfoMessagesV31{
		From:                     &mailjet.RecipientV31{Email: "", Name: ""},
		DeduplicateCampaign:      false,
		StatisticsContactsListID: 0,
		Subject:                  "Your account is about to expire",
		TemplateErrorDeliver:     false,
		TemplateID:               0,
		TemplateLanguage:         true,
		To:                       &mailjet.RecipientsV31{{Email: "john.2@metronom.com", Name: "John Doe 2"}},
		Variables: map[string]interface{}{
			"CustomerName": "John Doe 2", "DeletionDate": email.FormatDate(recipients[1].DeletionDate),
			"DeletionInDays": "11", "DeletionPreventionInDays": "10",
		},
	}

	assert.True(t, reflect.DeepEqual(expected2, result[1]), "Wrong result, expected2 != result[1]")

	expected3 := mailjet.InfoMessagesV31{
		From:                     &mailjet.RecipientV31{Email: "", Name: ""},
		DeduplicateCampaign:      false,
		StatisticsContactsListID: 0,
		Subject:                  "Your account is about to expire",
		TemplateErrorDeliver:     false,
		TemplateID:               0,
		TemplateLanguage:         true,
		To:                       &mailjet.RecipientsV31{{Email: "john.3@metronom.com", Name: "John Doe 3"}},
		Variables: map[string]interface{}{
			"CustomerName": "John Doe 3", "DeletionDate": email.FormatDate(recipients[2].DeletionDate),
			"DeletionInDays": "12", "DeletionPreventionInDays": "11",
		},
	}

	assert.True(t, reflect.DeepEqual(expected3, result[2]), "Wrong result, expected3 != result[2]")
}

func TestCreateDeleteMessageInfo(t *testing.T) {
	client := email.Client{}
	deletionDate := time.Now().Add(240 * time.Hour)
	result := client.CreateDeleteMessageInfo("john@metronom.com", "John Doe", deletionDate)

	expected := mailjet.InfoMessagesV31{
		From:                     &mailjet.RecipientV31{Email: "", Name: ""},
		DeduplicateCampaign:      false,
		StatisticsContactsListID: 0,
		Subject:                  "Your account is about to expire",
		TemplateErrorDeliver:     false,
		TemplateID:               0,
		TemplateLanguage:         true,
		To:                       &mailjet.RecipientsV31{{Email: "john@metronom.com", Name: "John Doe"}},
		Variables: map[string]interface{}{
			"CustomerName": "John Doe", "DeletionDate": email.FormatDate(deletionDate),
			"DeletionInDays": "10", "DeletionPreventionInDays": "9",
		},
	}

	assert.True(t, reflect.DeepEqual(expected, result), "Wrong result.")
}
