package email

import (
	"math"
	"strconv"
	"time"

	mailjet "github.com/mailjet/mailjet-apiv3-go"
	"github.com/pkg/errors"
	"gitlab.com/telo_tade/golang_micro_service/pkg/models"
)

// DeletionNotificationSubject stores the subject of our deletion notification email.
const DeletionNotificationSubject = "Your account is about to expire"

// Client can be used to interact with the Mailjet email service and send emails.
type Client struct {
	PublicKey   string
	SecretKey   string
	SenderName  string
	SenderEmail string
	TemplateID  int
}

// SendDeleteNotification will send a delete notification email.
func (c *Client) SendDeleteNotification(notifications []models.DeleteNotification) (err error) {
	messagesInfo := c.ComputeDeleteMessages(notifications)

	_, err = c.sendBulkMessages(messagesInfo)
	if err != nil {
		return errors.WithMessage(err, "Failed to send bulk messages")
	}

	return nil
}

// sendBulkMessages dispatches a batch of email messages using the Mailjet service.
func (c *Client) sendBulkMessages(input []mailjet.InfoMessagesV31) (result *mailjet.ResultsV31, err error) {
	mailjetClient := mailjet.NewMailjetClient(c.PublicKey, c.SecretKey)
	messages := mailjet.MessagesV31{Info: input}

	result, err = mailjetClient.SendMailV31(&messages)
	if err != nil {
		return result, errors.Wrap(err, "error sending the bulk messages")
	}

	return result, nil
}

// ComputeDeleteMessages creates an InfoMessagesV31 object for each recipient.
func (c *Client) ComputeDeleteMessages(input []models.DeleteNotification) (messages []mailjet.InfoMessagesV31) {
	messages = make([]mailjet.InfoMessagesV31, len(input))

	for i, r := range input {
		message := c.CreateDeleteMessageInfo(r.Email, r.Name, r.DeletionDate)

		messages[i] = message
	}

	return messages
}

// CreateDeleteMessageInfo produces the payload of a delete message.
func (c *Client) CreateDeleteMessageInfo(email, name string, deletionDate time.Time) (payload mailjet.InfoMessagesV31) {
	return mailjet.InfoMessagesV31{
		From: &mailjet.RecipientV31{
			Email: c.SenderEmail,
			Name:  c.SenderName,
		},
		To: &mailjet.RecipientsV31{
			mailjet.RecipientV31{
				Email: email,
				Name:  name,
			},
		},
		TemplateID:       c.TemplateID,
		TemplateLanguage: true,
		Subject:          DeletionNotificationSubject,
		Variables: map[string]interface{}{
			"DeletionPreventionInDays": strconv.Itoa((int)(math.Round(time.Until(deletionDate).Hours()/24) - 1)),
			"DeletionDate":             FormatDate(deletionDate),
			"CustomerName":             name,
			"DeletionInDays":           strconv.Itoa((int)(math.Round(time.Until(deletionDate).Hours() / 24))),
		},
	}
}

// FormatDate converts a supplied date into a human readable representation of that date.
// The day of the month will have the correct suffix appended (st,nd or rd).
func FormatDate(t time.Time) (formatted string) {
	suffix := "th"

	switch t.Day() {
	case 1, 21, 31:
		suffix = "st"
	case 2, 22:
		suffix = "nd"
	case 3, 23:
		suffix = "rd"
	}

	return t.Format("Monday 2" + suffix + " January 2006")
}
