package db

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/telo_tade/golang_micro_service/pkg/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// MySQLClient holds the internal connection to the database.
type MySQLClient struct {
	DB *gorm.DB
}

// NewMySQLClient establishes an initial connection to the database and sets up the schema.
func NewMySQLClient(host string, port uint, username string, password string, database string) (*MySQLClient, error) {
	con, err := createMySQLConnection(host, port, username, password, database)
	if err != nil {
		return nil, err
	}

	client := &MySQLClient{DB: con}

	if err := client.migrateDB(); err != nil {
		return nil, err
	}

	return client, nil
}

// migrateDB sets up the schema. No data will be deleted/transformed during this process.
func (client *MySQLClient) migrateDB() error {
	err := client.DB.AutoMigrate(&models.User{})
	if err != nil {
		return errors.Wrap(err, "Could not create the schema")
	}

	return nil
}

// createMySQLConnection establishes the connection with the PostgreSQL DB.
func createMySQLConnection(host string, port uint, username, password, database string) (*gorm.DB, error) {
	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?"+
		"charset=utf8mb4&parseTime=True&loc=Local",
		username, password, host, port, database)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, errors.Wrap(err, "Could not establish the DB connection")
	}

	return db, nil
}

// CreateUser adds a new user to the DB.
func (client *MySQLClient) CreateUser(ctx context.Context, p models.User) error {
	p.InsertedAt = time.Now()

	res := client.DB.WithContext(ctx).Create(&p)
	if res.Error != nil {
		return errors.Wrap(res.Error, "Could not create a user")
	}

	return nil
}

// GetUser retrieves a user from the DB based on the supplied user ID.
func (client *MySQLClient) GetUser(ctx context.Context, id uint) (models.User, error) {
	var r models.User

	res := client.DB.WithContext(ctx).Where("id = ?", id).Find(&r)
	if res.Error != nil || res.RowsAffected == 0 {
		return models.User{}, errors.New(fmt.Sprintf("Could not find a user with ID %v.", id))
	}

	return r, nil
}

// UpdateUser updates a given user entry inside the DB.
func (client *MySQLClient) UpdateUser(ctx context.Context, p models.User) error {
	res := client.DB.WithContext(ctx).Model(p).Updates(p)
	if res.Error != nil {
		return errors.Wrap(res.Error, "Could not update the user")
	}

	return nil
}

// DeleteUser deletes a given user entry from the DB.
func (client *MySQLClient) DeleteUser(ctx context.Context, p models.User) error {
	res := client.DB.WithContext(ctx).Delete(p)
	if res.Error != nil {
		return errors.Wrap(res.Error, "Could not delete the user")
	}

	return nil
}

// GetAllUsers retrieves all users from the database that are not soft-deleted.
func (client *MySQLClient) GetAllUsers(ctx context.Context) ([]models.User, error) {
	var r []models.User

	res := client.DB.WithContext(ctx).Find(&r)
	if res.Error != nil {
		return nil, errors.Wrap(res.Error, "Could not find any users")
	}

	return r, nil
}

// GetLatestUser retrieves the most recently added user from DB.
func (client *MySQLClient) GetLatestUser(ctx context.Context) (models.User, error) {
	var r models.User

	res := client.DB.WithContext(ctx).Order("inserted_at desc").Limit(1).First(&r)
	if res.Error != nil || res.RowsAffected == 0 {
		return models.User{}, errors.New("Could not find a user")
	}

	return r, nil
}
