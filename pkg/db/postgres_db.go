package db

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/telo_tade/golang_micro_service/pkg/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// PostgresClient holds the internal connection to the database.
type PostgresClient struct {
	DB *gorm.DB
}

// NewClient establishes an initial connection to the database and sets up the schema.
func NewClient(host string, port uint, username string, password string, database string) (*PostgresClient, error) {
	con, err := createConnection(host, port, username, password, database)
	if err != nil {
		return nil, err
	}

	client := &PostgresClient{DB: con}

	if err := client.migrateDB(); err != nil {
		return nil, err
	}

	return client, nil
}

// migrateDB sets up the schema. No data will be deleted/transformed during this process.
func (client *PostgresClient) migrateDB() error {
	err := client.DB.AutoMigrate(&models.User{})
	if err != nil {
		return errors.Wrap(err, "Could not create the schema")
	}

	return nil
}

// createConnection establishes the connection with the PostgreSQL DB.
func createConnection(host string, port uint, username string, password string, database string) (*gorm.DB, error) {
	db, err := gorm.Open(postgres.Open(fmt.Sprintf("host=%v port=%v user=%v password=%v dbname=%v sslmode=disable",
		host, port, username, password, database)), &gorm.Config{})
	if err != nil {
		return nil, errors.Wrap(err, "Could not establish the DB connection")
	}

	return db, nil
}

// CreateUser adds a new user to the DB.
func (client *PostgresClient) CreateUser(ctx context.Context, p models.User) error {
	p.InsertedAt = time.Now()

	res := client.DB.WithContext(ctx).Create(&p)
	if res.Error != nil {
		return errors.Wrap(res.Error, "Could not create a user")
	}

	return nil
}

// GetUser retrieves a user from the DB based on the supplied user ID.
func (client *PostgresClient) GetUser(ctx context.Context, id uint) (models.User, error) {
	var r models.User

	res := client.DB.WithContext(ctx).Where("id = ?", id).Find(&r)
	if res.Error != nil || res.RowsAffected == 0 {
		return models.User{}, errors.New(fmt.Sprintf("Could not find a user with ID %v.", id))
	}

	return r, nil
}

// UpdateUser updates a given user entry inside the DB.
func (client *PostgresClient) UpdateUser(ctx context.Context, p models.User) error {
	res := client.DB.WithContext(ctx).Model(p).Updates(p)
	if res.Error != nil {
		return errors.Wrap(res.Error, "Could not update the user")
	}

	return nil
}

// DeleteUser deletes a given user entry from the DB.
func (client *PostgresClient) DeleteUser(ctx context.Context, p models.User) error {
	res := client.DB.WithContext(ctx).Delete(p)
	if res.Error != nil {
		return errors.Wrap(res.Error, "Could not delete the user")
	}

	return nil
}

// GetAllUsers retrieves all users from the database that are not soft-deleted.
func (client *PostgresClient) GetAllUsers(ctx context.Context) ([]models.User, error) {
	var r []models.User

	res := client.DB.WithContext(ctx).Find(&r)
	if res.Error != nil {
		return nil, errors.Wrap(res.Error, "Could not find any users")
	}

	return r, nil
}

// GetLatestUser retrieves the most recently added user from DB.
func (client *PostgresClient) GetLatestUser(ctx context.Context) (models.User, error) {
	var r models.User

	res := client.DB.WithContext(ctx).Order("inserted_at desc").Limit(1).First(&r)
	if res.Error != nil || res.RowsAffected == 0 {
		return models.User{}, errors.New("Could not find a user")
	}

	return r, nil
}

// UpdateLastNotified updates a given project's lastNotified field.
func (client *PostgresClient) UpdateLastNotified(ctx context.Context, userID int, deletionAt time.Time) error {
	res := client.DB.WithContext(ctx).Model(models.User{}).Where("id = ?", userID).Update("deletion_at", deletionAt)
	if res.Error != nil {
		return errors.Wrap(res.Error, "could not update last_notified of user")
	}

	return nil
}
