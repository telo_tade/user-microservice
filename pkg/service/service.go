package service

import (
	"context"
	"fmt"
	"time"

	"github.com/go-log/log"
	"gitlab.com/telo_tade/golang_micro_service/pkg/models"
)

// Service hold the business logic.
type Service struct {
	Logger      log.Logger
	DBClient    dbClient
	MailClient  mailjet
	TeamsClient teams
}

// mailjet models the ability to send emails.
type mailjet interface {
	SendDeleteNotification(notifications []models.DeleteNotification) (err error)
}

// teams models functionality to push to Teams.
type teams interface {
	SendTeamsNotification(msg string) (err error)
}

// dbClient models interaction with DB.
type dbClient interface {
	GetUser(ctx context.Context, id uint) (models.User, error)
	GetAllUsers(ctx context.Context) ([]models.User, error)
	CreateUser(ctx context.Context, p models.User) error
	GetLatestUser(ctx context.Context) (models.User, error)
	UpdateLastNotified(ctx context.Context, userID int, deletionAt time.Time) error
}

// CreateUser creates a new user.
func (s *Service) CreateUser(ctx context.Context, u models.User) error {
	err := s.DBClient.CreateUser(ctx, u)
	if err != nil {
		s.Logger.Logf("Error creating user: %v.", err)

		return err
	}

	return nil
}

// GetUser returns the user having a given ID.
func (s *Service) GetUser(ctx context.Context, userID uint) (*models.User, error) {
	user, err := s.DBClient.GetUser(ctx, userID)
	if err != nil {
		s.Logger.Logf("Error getting user from db, msg: %v.", err)

		return nil, err
	}

	return &user, nil
}

// GetAllUsers returns all users.
func (s *Service) GetAllUsers(ctx context.Context) ([]models.User, error) {
	users, err := s.DBClient.GetAllUsers(ctx)
	if err != nil {
		s.Logger.Logf("Error getting all users %v.", err)

		return nil, err
	}

	return users, nil
}

// GetLatestUser returns the user having a given ID.
func (s *Service) GetLatestUser(ctx context.Context) (*models.User, error) {
	user, err := s.DBClient.GetLatestUser(ctx)
	if err != nil {
		s.Logger.Logf("Error getting user from db, msg: %v.", err)

		return nil, err
	}

	return &user, nil
}

// ExecuteFlow creates a new user and saves it, send teams & email notifications, update last notified.
// This is all play, no business value, emailed user is harcoded.
func (s *Service) ExecuteFlow() {
	now := time.Now()
	user := models.User{
		Name:       fmt.Sprintf("User %v", now.Unix()),
		DeletionAt: now.Add(24 * 60 * time.Second),
		Email:      fmt.Sprintf("user_%v@gmail.com", now.Unix()),
	}

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	createError := s.DBClient.CreateUser(ctx, user)
	if createError != nil {
		s.Logger.Logf("Error creating user: %v.", createError)
	}

	alertError := s.TeamsClient.SendTeamsNotification(fmt.Sprintf("Created a new user: %v", user))
	if alertError != nil {
		s.Logger.Logf("Error pushing to teams: %v", alertError)
	}

	toBeDeleted := []models.DeleteNotification{
		{
			Email:        "test.email@gmail.com",
			DeletionDate: time.Now().Add(24 * time.Hour),
			LastNotified: time.Now().Add(-1 * time.Hour),
			Name:         "John Doe",
		},
	}

	emailError := s.MailClient.SendDeleteNotification(toBeDeleted)
	if emailError != nil {
		s.Logger.Logf("Error pushing to teams: %v", alertError)
	} else {
		err := s.DBClient.UpdateLastNotified(context.Background(), 22, time.Now())
		if err != nil {
			s.Logger.Logf("Cannot updateLastNotified: %v", err)
		}
	}
}
