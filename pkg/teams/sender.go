package teams

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/go-log/log"
	"github.com/pkg/errors"
)

// Client encapsulates functionality to send Teams notifications.
type Client struct {
	Log     log.Logger
	Webhook string
}

// RequestBody models the body of a request to push to Teams.
type RequestBody struct {
	Text string `json:"text"`
}

// SendTeamsNotification will post a message to a Teams channel.
func (c *Client) SendTeamsNotification(msg string) (err error) {
	return c.sendNotification(ReplaceNewLines(msg))
}

// ReplaceNewLines returns a string which has \n replaced by <br>.
func ReplaceNewLines(msg string) (result string) {
	replacer := strings.NewReplacer("\n", "<br>")

	return replacer.Replace(msg)
}

// sendNotification will post a message to the channel the webhook points to.
func (c *Client) sendNotification(msg string) (err error) {
	slackBody, _ := json.Marshal(RequestBody{Text: msg})

	req, err := http.NewRequestWithContext(context.Background(), http.MethodPost, c.Webhook, bytes.NewBuffer(slackBody))
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{Timeout: 10 * time.Second}

	resp, err := client.Do(req)
	if err != nil {
		c.Log.Logf("Error pushing to Teams: %v", err)

		return err
	}

	defer func(err *error) {
		*err = resp.Body.Close()
	}(&err)

	buf := new(bytes.Buffer)

	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return err
	}

	if buf.String() != "ok" && buf.String() != "1" {
		c.Log.Logf("buf: %v", buf.String())

		return errors.Errorf("Non-ok response returned from Slack")
	}

	return err
}
