package teams_test

import (
	"testing"

	"gitlab.com/telo_tade/golang_micro_service/pkg/teams"
)

func TestReplaceNewLines(t *testing.T) {
	type args struct {
		msg string
	}

	tests := []struct {
		name       string
		args       args
		wantResult string
	}{
		{
			name: "One newline",
			args: args{msg: `Notification message
			Your project is about to be deleted.`},
			wantResult: "Notification message<br>\t\t\tYour project is about to be deleted.",
		},
		{
			name:       "Single line",
			args:       args{msg: "Notification\nmessage\nregarding project."},
			wantResult: "Notification<br>message<br>regarding project.",
		},
		{
			name:       "No newlines",
			args:       args{msg: "Notification message regarding project."},
			wantResult: "Notification message regarding project.",
		},
		{
			name:       "Empty string",
			args:       args{msg: ""},
			wantResult: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := teams.ReplaceNewLines(tt.args.msg); gotResult != tt.wantResult {
				t.Errorf("ReplaceNewLines() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}
