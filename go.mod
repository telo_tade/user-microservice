module gitlab.com/telo_tade/golang_micro_service

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/docker/docker v1.13.1
	github.com/go-log/log v0.2.0
	github.com/google/subcommands v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/mailjet/mailjet-apiv3-go v0.0.0-20190724151621-55e56f74078c
	github.com/pkg/errors v0.9.1
	github.com/robfig/cron v1.2.0
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.5.1
	golang.org/x/exp/errors v0.0.0-20200917184745-18d7dbdd5567
	gopkg.in/yaml.v2 v2.3.0
	gorm.io/driver/mysql v1.0.1
	gorm.io/driver/postgres v1.0.0
	gorm.io/gorm v1.20.1
)
