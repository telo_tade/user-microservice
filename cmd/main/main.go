package main

import (
	"context"
	"flag"
	"os"

	"github.com/google/subcommands"
	"gitlab.com/telo_tade/golang_micro_service/pkg/commands"
	configsreader "gitlab.com/telo_tade/golang_micro_service/pkg/configs"
	"gitlab.com/telo_tade/golang_micro_service/pkg/db"
	"gitlab.com/telo_tade/golang_micro_service/pkg/email"
	"gitlab.com/telo_tade/golang_micro_service/pkg/logger"
	"gitlab.com/telo_tade/golang_micro_service/pkg/rest"
	"gitlab.com/telo_tade/golang_micro_service/pkg/scheduler"
	"gitlab.com/telo_tade/golang_micro_service/pkg/service"
	"gitlab.com/telo_tade/golang_micro_service/pkg/teams"
)

// GitCommit is used to inject the commit number.
var GitCommit = "local_build"

// Version is used to inject the version number.
var Version = "unknown"

func main() {
	configs, err := configsreader.ReadConfigs()
	if err != nil {
		panic(err.Error())
	}

	l := logger.InitLogger()
	l.Logf("µS - commit: %v, version: %v", GitCommit, Version)

	db, err := db.NewClient(configs.DBHost, configs.DBPort, configs.Username, configs.Password, configs.Database)
	if err != nil {
		panic(err.Error())
	}

	mail := email.Client{
		PublicKey: configs.EmailPublicKey, SecretKey: configs.EmailSecretKey, TemplateID: configs.TemplateID,
		SenderEmail: configs.EmailSenderEmail, SenderName: configs.EmailSenderName,
	}
	teamsClient := teams.Client{Log: l, Webhook: configs.TeamsWebhook}
	service := service.Service{Logger: l, DBClient: db, MailClient: &mail, TeamsClient: &teamsClient}
	handler := rest.Handler{Logger: l, Service: service}
	server := rest.NewServer(configs.Host, configs.Port, l, handler)
	scheduler := scheduler.Client{CronExpression: configs.Schedule, Flow: &service, Logger: l}

	subcommands.Register(subcommands.HelpCommand(), "")
	subcommands.Register(subcommands.FlagsCommand(), "")
	subcommands.Register(subcommands.CommandsCommand(), "")

	subcommands.Register(&commands.APICmd{
		Server: server,
	}, "")

	subcommands.Register(&commands.JobCmd{
		Scheduler: &scheduler,
	}, "")

	flag.Parse()

	ctx := context.Background()
	os.Exit(int(subcommands.Execute(ctx)))
}
